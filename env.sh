#!/bin/bash
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
cd gist
python3 manage.py runserver 0.0.0.0:8000