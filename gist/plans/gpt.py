from datetime import datetime
from .models import *

import openai
openai.api_key = "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"

def save_suggestions(user, Object, suggestions):
    
    for suggestion in suggestions:

        new_suggestion = Suggestion()
        
        object = Object.__class__.__name__

        if "Goal" == object:
            new_suggestion.goal = Object

        if "Idea" == object:
            new_suggestion.idea = Object

        if "Step" == object:
            new_suggestion.step = Object

        if "Task" == object:
            new_suggestion.task = Object

        new_suggestion.object_type = f'{object}'
        new_suggestion.object_reserved_name = Object.name
        new_suggestion.object_id = Object.pk
        new_suggestion.status = "unknown"
        new_suggestion.value = suggestion
        new_suggestion.created = datetime.now()
        new_suggestion.updated = datetime.now()
        new_suggestion.created_by = user
        new_suggestion.save()

def get_suggestions(user, Object):

    completion = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "user", 
            "content": f"""
            How to solve the following problem: 
            {Object.name}: {Object.body};
            Answer should be in the 4-5 steps. 
            Each step can't be more than 7 words. 
            Each step should be displayed into 1 sentence. 
            No additional information and comments. """}
        ]
    )

    suggestions = completion.choices[0].message.content.split('\n')
    print(suggestions)

    save_suggestions(user, Object, suggestions)
