from django.conf import settings
from django.db import models

class Status(models.Model):
    name = models.CharField(max_length=40)
    body = models.TextField()
    final = models.BooleanField(default=False) # TRUE for finish steps like "RESOLVED"
    color = models.CharField(max_length=10)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Goal(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    body = models.TextField()
    status = models.ForeignKey(Status, related_name="goals", on_delete=models.SET_NULL, null=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Idea(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    body = models.TextField()
    status = models.ForeignKey(Status, related_name="ideas", on_delete=models.SET_NULL, null=True)
    goal = models.ForeignKey(
        Goal, related_name="ideas", on_delete=models.CASCADE
    )
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Step(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    body = models.TextField()
    status = models.ForeignKey(Status, related_name="steps", on_delete=models.SET_NULL, null=True)
    idea = models.ForeignKey(
        Idea, related_name="steps", on_delete=models.CASCADE
    )
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Task(models.Model):
    name = models.CharField(max_length=200)
    order = models.IntegerField(default=0)
    body = models.TextField()
    status = models.ForeignKey(Status, related_name="tasks", on_delete=models.SET_NULL, null=True)
    step = models.ForeignKey(Step, related_name="tasks", on_delete=models.CASCADE)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    
class Suggestion(models.Model):

    goal = models.ForeignKey(Goal, related_name="suggestions", on_delete=models.DO_NOTHING, null=True, default=None)
    idea = models.ForeignKey(Idea, related_name="suggestions", on_delete=models.DO_NOTHING, null=True, default=None)
    step = models.ForeignKey(Step, related_name="suggestions", on_delete=models.DO_NOTHING, null=True, default=None)
    task = models.ForeignKey(Task, related_name="suggestions", on_delete=models.DO_NOTHING, null=True, default=None)

    OBJECTS = (
        ("goal", "Goal"),
        ("idea", "Idea"),
        ("step", "Step"),
        ("task", "Task"),
    )

    object_type = models.CharField(
        max_length=20,
        choices = OBJECTS,
        default = 'goal',
        null=False
    )

    object_reserved_name = models.TextField(null=False)
    object_id = models.IntegerField(null=False)

    STATUSES = (
        ("archived", "Archived"),
        ("in_use", "Used"),
        ("unknown", "Unknown")
    )

    status = models.CharField(
        max_length=20,
        choices=STATUSES,
        default='unknown',
        null=False
    )

    value = models.TextField(null=False)

    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.value