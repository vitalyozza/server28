from datetime import datetime
import graphene
from graphene_django import DjangoObjectType

from .models import *
from users.schema import UserType
from .gpt import *

# TYPES

class GoalType(DjangoObjectType):
    class Meta:
        model = Goal
        fields = "__all__"

class IdeaType(DjangoObjectType):
    class Meta:
        model = Idea
        fields = "__all__"

class StepType(DjangoObjectType):
    class Meta:
        model = Step
        fields = "__all__"

class TaskType(DjangoObjectType):
    class Meta:
        model = Task
        fields = "__all__"

class StatusType(DjangoObjectType):
    class Meta:
        model = Status
        fields = "__all__"

class SuggestionType(DjangoObjectType):
    class Meta:
        model = Suggestion
        fields = "__all__"

class Query(graphene.ObjectType):

    # STATUSES
    all_statuses = graphene.List(StatusType)
    get_status_by_id = graphene.Field(StatusType, id=graphene.ID(required=True))

    def resolve_all_statuses(self, info):
        return Status.objects.all()

    def resolve_get_status_by_id(root, info, id):
        return Status.objects.get(pk=id)


    # GOALS
    all_goals = graphene.List(GoalType)
    get_goal_by_id = graphene.Field(GoalType, id=graphene.ID(required=True))
    get_goal_by_status = graphene.Field(GoalType, status=graphene.ID(required=True))

    def resolve_all_goals(self, info):
        return Goal.objects.all()

    def resolve_get_goal_by_id(root, info, id):
        return Goal.objects.get(pk=id)

    def resolve_get_goal_by_status(root, info, status):
        return Goal.objects.get(status_id=status)


    # IDEAS
    all_ideas = graphene.List(IdeaType)
    get_idea_by_id = graphene.Field(IdeaType, id=graphene.ID(required=True))
    get_idea_by_status = graphene.Field(IdeaType, status=graphene.ID(required=True))
    get_ideas_by_goal = graphene.List(IdeaType, id=graphene.ID(required=True))

    def resolve_all_ideas(root, info):
        return Idea.objects.all()

    def resolve_get_idea_by_id(root, info, id):
        return Idea.objects.get(pk=id)

    def resolve_get_idea_by_status(root, info, status):
        return Idea.objects.get(status_id=status)

    def resolve_get_ideas_by_goal(root, info, id):
        current_goal = Goal.objects.get(pk=id)
        return current_goal.ideas.all()

    # STEPS
    all_steps = graphene.List(StepType)
    get_step_by_id = graphene.Field(StepType, id=graphene.ID(required=True))
    get_step_by_status = graphene.Field(StepType, status=graphene.ID(required=True))
    get_steps_by_idea = graphene.List(StepType, id=graphene.ID(required=True))

    def resolve_all_steps(root, info):
        return Step.objects.all()    
    
    def resolve_get_step_by_id(root, info, id):
        return Step.objects.get(pk=id)

    def resolve_get_step_by_status(root, info, status):
        return Step.objects.get(status_id=status)

    def resolve_get_steps_by_idea(root, info, id):
        current_idea = Idea.objects.get(pk=id)
        return current_idea.steps.all()

    # TASKS
    all_tasks = graphene.List(TaskType)
    get_task_by_id = graphene.Field(TaskType, id=graphene.ID(required=True))
    get_tasks_by_status = graphene.List(TaskType, status=graphene.ID(required=True))
    get_tasks_by_step = graphene.List(TaskType, id=graphene.ID(required=True))
    

    def resolve_all_tasks(root, info):
        return Task.objects.all()

    def resolve_get_task_by_id(root, info, id):
        return Task.objects.get(pk=id)

    def resolve_get_task_by_status(root, info, status):
        return Task.objects.get(status_id=status)

    def resolve_get_tasks_by_step(root, info, id):
        current_step = Step.objects.get(pk=id)
        return current_step.tasks.all()

# STATUS
class CreateStatus(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        final = graphene.Boolean(required=True)
        color = graphene.String(required=True)

    status = graphene.Field(StatusType)

    @classmethod
    def mutate(cls, root, info, name, body, final=False, color=None):
        user = info.context.user or None

        status = Status()
        status.name = name
        status.body = body
        status.final = final
        status.color = color
        status.created = datetime.now()
        status.updated = datetime.now()
        status.created_by = user
        status.save()

        return CreateStatus(status=status)

class UpdateStatus(graphene.Mutation):

    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        final = graphene.Boolean(required=False, default_value=None)
        color = graphene.String(required=False, default_value=None)

    status = graphene.Field(StatusType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, final=None, color=None):
        status = Status.objects.get(pk=id)
        if name is not None:
            status.name = name
        if body is not None:
            status.body = body

        if final is not None:
            status.final = final

        if color is not None:
            status.color = color

        status.updated = datetime.now()
        status.save()

        return UpdateStatus(status=status)

class RemoveStatus(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    status = graphene.Field(StatusType)

    @classmethod
    def mutate(cls, root, info, id):
        status = Status.objects.get(pk=id)
        status.delete()
        status.id = id
        return RemoveStatus(status=status)

# GOALS
class CreateGoal(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        status = graphene.ID(required=False)

    goal = graphene.Field(GoalType)

    @classmethod
    def mutate(cls, root, info, name, body, status=None):
        user = info.context.user or None

        goal = Goal()
        goal.name = name
        goal.body = body

        if status is not None:
            status_obj = Status.objects.get(id=status)
            goal.status = status_obj

        goal.order = 0
        goal.created = datetime.now()
        goal.updated = datetime.now()
        goal.created_by = user
        goal.save()

        get_suggestions(user, Object=goal)

        return CreateGoal(goal=goal)

class UpdateGoal(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        status = graphene.ID(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        order = graphene.Int(required=False, default_value=None)

    goal = graphene.Field(GoalType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, status=None, order=None):
        goal = Goal.objects.get(pk=id)
        if name is not None:
            goal.name = name
        if body is not None:
            goal.body = body

        if status is not None:
            status_obj = Status.objects.get(id=status)
            goal.status = status_obj

        if order is not None:
            goal.order = order
        goal.updated = datetime.now()
        goal.save()

        return UpdateGoal(goal=goal)


class RemoveGoal(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    goal = graphene.Field(GoalType)

    @classmethod
    def mutate(cls, root, info, id):
        goal = Goal.objects.get(pk=id)
        goal.delete()
        goal.id = id
        return RemoveGoal(goal=goal)


# IDEAS
class CreateIdea(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        status = graphene.ID(required=False, default_value=None)
        goal_pk = graphene.ID(required=True)
        
    idea = graphene.Field(IdeaType)

    @classmethod
    def mutate(cls, root, info, name, body, goal_pk, status=None):
        user = info.context.user or None

        idea = Idea()
        idea.name = name
        idea.body = body

        if status is not None:
            status_obj = Status.objects.get(id=status)
            idea.status = status_obj

        idea.order = 0
        idea.goal = Goal.objects.get(pk=goal_pk)
        idea.created = datetime.now()
        idea.updated = datetime.now()
        idea.created_by = user
        idea.save()

        get_suggestions(user, Object=idea)

        return CreateIdea(idea=idea)

class UpdateIdea(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        status = graphene.ID(required=False, default_value=None)
        order = graphene.Int(required=False, default_value=None)
        goal_pk = graphene.ID(required=False, default_value=None)

    idea = graphene.Field(IdeaType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, status=None, order=None, goal_pk=None):
        idea = Idea.objects.get(pk=id)
        if name is not None:
            idea.name = name
        if body is not None:
            idea.body = body
        if status is not None:
            status_obj = Status.objects.get(id=status)
            idea.status = status_obj
        if order is not None:
            idea.order = order
        if goal_pk is not None:
            idea.goal = Goal.objects.get(pk=goal_pk)
        idea.updated = datetime.now()
        idea.save()

        return UpdateIdea(idea=idea)

class RemoveIdea(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    idea = graphene.Field(IdeaType)

    @classmethod
    def mutate(cls, root, info, id):
        idea = Idea.objects.get(pk=id)
        idea.delete()
        idea.id = id
        return RemoveIdea(idea=idea)

# STEPS
class CreateStep(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        status = graphene.ID(required=False, default_value=None)
        idea_pk = graphene.ID(required=True)

    step = graphene.Field(StepType)

    @classmethod
    def mutate(cls, root, info, name, body, idea_pk, status=None):
        user = info.context.user or None

        step = Step()
        step.name = name
        step.body = body
        step.order = 0
        if status is not None:
            status_obj = Status.objects.get(id=status)
            step.status = status_obj
        step.created_by = user
        step.idea = Idea.objects.get(pk=idea_pk)
        step.created = datetime.now()
        step.updated = datetime.now()

        step.save()

        get_suggestions(user, Object=step)

        return CreateStep(step=step)

class UpdateStep(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        status = graphene.ID(required=False, default_value=None)
        order = graphene.Int(required=False, default_value=None)
        priority = graphene.String(required=False, default_value=None)
        idea_pk = graphene.ID(required=False, default_value=None)

    step = graphene.Field(StepType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, status=None, order=None, priority=None, idea_pk=None):
        step = Step.objects.get(pk=id)
        if name is not None:
            step.name = name
        if body is not None:
            step.body = body
        if status is not None:
            status_obj = Status.objects.get(id=status)
            step.status = status_obj
        if order is not None:
            step.order = order
        if priority is not None:
            step.priority = priority
        if idea_pk is not None:
            step.idea = Idea.objects.get(pk=idea_pk)
        step.updated = datetime.now()
        step.save()

        return UpdateStep(step=step)

class RemoveStep(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    step = graphene.Field(StepType)

    @classmethod
    def mutate(cls, root, info, id):
        step = Step.objects.get(pk=id)
        step.delete()
        step.id = id
        return RemoveStep(step=step)

# TASKS
class CreateTask(graphene.Mutation):

    class Arguments:
        name = graphene.String(required=True)
        body = graphene.String(required=True)
        status = graphene.ID(required=False, default_value=None)
        step_pk = graphene.ID(required=True)

    task = graphene.Field(TaskType)

    @classmethod
    def mutate(cls, root, info, name, body, step_pk, status=None):
        user = info.context.user or None
        task = Task()
        task.name = name
        task.body = body
        task.order = 0

        if status is not None:
            status_obj = Status.objects.get(id=status)
            task.status = status_obj

        task.step = Step.objects.get(pk=step_pk)
        task.created = datetime.now()
        task.updated = datetime.now()
        task.created_by = user
        task.save()

        get_suggestions(user, Object=task)

        return CreateTask(task=task)    

class UpdateTask(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        name = graphene.String(required=False, default_value=None)
        body = graphene.String(required=False, default_value=None)
        status = graphene.ID(required=False, default_value=None)
        order = graphene.Int(required=False, default_value=None)
        step_pk = graphene.ID(required=False, default_value=None)

    task = graphene.Field(TaskType)

    @classmethod
    def mutate(cls, root, info, id, name=None, body=None, order=None, status=None, step_pk=None):
        task = Task.objects.get(pk=id)
        if name is not None:
            task.name = name
        if body is not None:
            task.body = body
        if order is not None:
            task.order = order
        if status is not None:
            status_obj = Status.objects.get(id=status)
            task.status = status_obj
        if step_pk is not None:
            task.step = Step.objects.get(pk=step_pk)
        task.updated = datetime.now()
        task.save()

        return UpdateTask(task=task)

class RemoveTask(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    task = graphene.Field(TaskType)

    @classmethod
    def mutate(cls, root, info, id):
        task = Task.objects.get(pk=id)
        task.delete()
        task.id = id
        return RemoveTask(task=task)

class Mutation(graphene.ObjectType):

    # STATUSES
    create_status = CreateStatus.Field()
    update_status = UpdateStatus.Field()
    remove_remove = RemoveStatus.Field()

    # GOALS
    create_goal = CreateGoal.Field()
    update_goal = UpdateGoal.Field()
    remove_goal = RemoveGoal.Field()

    # IDEAS
    create_idea = CreateIdea.Field()
    update_idea = UpdateIdea.Field()
    remove_idea = RemoveIdea.Field()

    # STEPS
    create_step = CreateStep.Field()
    update_step = UpdateStep.Field()
    remove_step = RemoveStep.Field()

    # TASKS
    create_task = CreateTask.Field()
    update_task = UpdateTask.Field()
    remove_taks = RemoveTask.Field()
