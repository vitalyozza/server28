from django.contrib import admin
from plans.models import *
from users.models import *

admin.site.register(Status)
admin.site.register(Goal)
admin.site.register(Idea)
admin.site.register(Step)
admin.site.register(Task)
admin.site.register(Suggestion)